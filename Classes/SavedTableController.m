

#import "SavedTableController.h"
#import "PaintingViewController.h"
#import "PaintingView.h";

@interface SavedTableController ()

@end

@implementation SavedTableController
{
    NSArray *recipes;
    PaintingViewController *paintView;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    NSArray *keys = [[[NSUserDefaults standardUserDefaults] dictionaryRepresentation] allKeys];
    NSLog(@"%@", keys);
    
    NSMutableArray *correctKeys = [[NSMutableArray alloc] init];
    
    for(int i = 0; i < keys.count; i++){
        
        NSString *s = [keys objectAtIndex:i];
        NSString *s2 = [s substringToIndex:4];
        if([s2 isEqualToString:@"note"]){
            [correctKeys addObject:s];
            NSLog(@"here");
        }
    }
    // Initialize table data
    recipes = correctKeys;
    NSLog(@"%@", correctKeys);
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [recipes count];
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([segue.identifier isEqualToString:@"goPaint"]) {
        paintView = [segue destinationViewController];
        paintView.vc = self.vc;
    }
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    NSData *pathsdata = [[NSUserDefaults standardUserDefaults] objectForKey:[recipes objectAtIndex:indexPath.row]];
    NSArray *oldSavedArray = [NSKeyedUnarchiver unarchiveObjectWithData:pathsdata];
    
    NSLog(@"%@", oldSavedArray);
    
    uint8_t command[] = {0x04};
    NSData *nsData = [[NSData alloc] initWithBytes:&command length:1];
    [self.vc.peripheral writeValue:nsData forCharacteristic:self.vc.txCharacteristic type:CBCharacteristicWriteWithoutResponse];
    
    [self performSegueWithIdentifier:@"goPaint" sender:self];
    [(PaintingView*)paintView.view setPaths:oldSavedArray];
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *simpleTableIdentifier = @"SimpleTableCell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:simpleTableIdentifier];
    
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:simpleTableIdentifier];
    }
    
    cell.textLabel.text = [recipes objectAtIndex:indexPath.row];
  //  cell.imageView.image = [UIImage imageNamed:@"creme_brelee.jpg"];
    
    return cell;
}
@end
