

#import "PaintingViewController.h"
#import "PaintingView.h"
#import "SoundEffect.h"

//CONSTANTS:

#define kBrightness             1.0
#define kSaturation             0.75

#define kPaletteHeight			30
#define kPaletteSize			5
#define kMinEraseInterval		0.5

// Padding for margins
#define kLeftMargin				10.0
#define kTopMargin				10.0
#define kRightMargin			10.0

//CLASS IMPLEMENTATIONS:

@interface PaintingViewController()
{
	SoundEffect			*erasingSound;
	SoundEffect			*selectSound;
	CFTimeInterval		lastTime;    
}
@end


@implementation PaintingViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.pointsQueue = [[NSMutableArray alloc] init];
    
    // Create a segmented control so that the user can choose the brush color.
    // Create the UIImages with the UIImageRenderingModeAlwaysOriginal rendering mode. This allows us to show the actual image colors in the segmented control.
//    UISegmentedControl *segmentedControl = [[UISegmentedControl alloc] initWithItems:
//                                            [NSArray arrayWithObjects:
//                                             [[UIImage imageNamed:@"Red"]       imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal],
//                                             [[UIImage imageNamed:@"Yellow"]    imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal],
//                                             [[UIImage imageNamed:@"Green"]     imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal],
//                                             [[UIImage imageNamed:@"Blue"]      imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal],
//                                             [[UIImage imageNamed:@"Purple"]    imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal],
//                                             nil]];
//    
    // Compute a rectangle that is positioned correctly for the segmented control you'll use as a brush color palette
//    CGRect rect = [[UIScreen mainScreen] bounds];
//    CGRect frame = CGRectMake(rect.origin.x + kLeftMargin, rect.size.height - kPaletteHeight - kTopMargin, rect.size.width - (kLeftMargin + kRightMargin), kPaletteHeight);
//    segmentedControl.frame = frame;
//    // When the user chooses a color, the method changeBrushColor: is called.
//    [segmentedControl addTarget:self action:@selector(changeBrushColor:) forControlEvents:UIControlEventValueChanged];
//    // Make sure the color of the color complements the black background
//    segmentedControl.tintColor = [UIColor darkGrayColor];
//    // Set the third color (index values start at 0)
//    segmentedControl.selectedSegmentIndex = 2;
//    
//    // Add the control to the window
//    [self.view addSubview:segmentedControl];
//    // Now that the control is added, you can release it
    
    // Define a starting color
    CGColorRef color = [UIColor colorWithHue:(CGFloat)2.0 / (CGFloat)kPaletteSize
                                  saturation:kSaturation
                                  brightness:kBrightness
                                       alpha:1.0].CGColor;
    const CGFloat *components = CGColorGetComponents(color);
    
	// Defer to the OpenGL view to set the brush color
    [(PaintingView *)self.view setBrushColorWithRed:components[0] green:components[1] blue:components[2]];
    
    [(PaintingView *)self.view setVc:self.vc];
   
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(eraseView) name:@"shake" object:nil];
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    [self becomeFirstResponder];
}

- (BOOL)canBecomeFirstResponder
{
    return YES;
}

- (IBAction)sendDraw:(id)sender {
    [(PaintingView *)self.view sendBluetoothForSavedNote];
}


- (IBAction)clickSave:(id)sender
{
    [(PaintingView *)self.view saveNote];
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Saved Note"
                                                    message:@"Note has been successfully saved!!"
                                                   delegate:self
                                          cancelButtonTitle:@"OK"
                          
                                          otherButtonTitles:nil];
    [alert show];
}

// Release resources when they are no longer needed,
- (void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

-(void) viewWillDisappear:(BOOL)animated {
    if ([self.navigationController.viewControllers indexOfObject:self]==NSNotFound) {
        uint8_t command[] = {0x06};
        NSData *nsData = [[NSData alloc] initWithBytes:&command length:1];
        [self.vc.peripheral writeValue:nsData forCharacteristic:self.vc.txCharacteristic type:CBCharacteristicWriteWithoutResponse];
        NSLog(@"Sent: 0x06");
    }
    [super viewWillDisappear:animated];
}

// Change the brush color
- (void)changeBrushColor:(id)sender
{
    // Play sound
    [selectSound play];
    
    // Define a new brush color
    CGColorRef color = [UIColor colorWithHue:(CGFloat)[sender selectedSegmentIndex] / (CGFloat)kPaletteSize
                                  saturation:kSaturation
                                  brightness:kBrightness
                                       alpha:1.0].CGColor;
    const CGFloat *components = CGColorGetComponents(color);
    
    // Defer to the OpenGL view to set the brush color
    [(PaintingView *)self.view setBrushColorWithRed:components[0] green:components[1] blue:components[2]];
}

-(void) setPaintViewPaths : (NSArray*) paths {
    
    [(PaintingView *)self.view setPaths:paths];
}

// Called when receiving the "shake" notification; plays the erase sound and redraws the view
- (void)eraseView
{
	if(CFAbsoluteTimeGetCurrent() > lastTime + kMinEraseInterval) {
		[erasingSound play];
		[(PaintingView *)self.view erase];
		lastTime = CFAbsoluteTimeGetCurrent();
	}
}

// We do not support auto-rotation in this sample
- (BOOL)shouldAutorotate
{
    return NO;
}

#pragma mark Motion

- (void)motionEnded:(UIEventSubtype)motion withEvent:(UIEvent *)event
{
	if (motion == UIEventSubtypeMotionShake)
	{
		// User was shaking the device. Post a notification named "shake".
		[[NSNotificationCenter defaultCenter] postNotificationName:@"shake" object:self];
	}
}
@end
