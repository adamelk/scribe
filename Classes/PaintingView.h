
#import <UIKit/UIKit.h>
#import <OpenGLES/EAGL.h>
#import <OpenGLES/ES2/gl.h>
#import <OpenGLES/ES2/glext.h>
#import "RBLViewController.h"
#import "PaintingViewController.h"

//CLASS INTERFACES:

@interface PaintingView : UIView

@property(nonatomic, readwrite) CGPoint location;
@property(nonatomic, readwrite) CGPoint previousLocation;
@property (nonatomic, strong) RBLViewController *vc;
@property (nonatomic, strong) NSMutableArray *pointsQueue;
@property (nonatomic, strong) NSArray *paths;
@property (nonatomic, strong) PaintingViewController *paintingView;


- (void)erase;
- (void)saveNote;
- (void)setBrushColorWithRed:(CGFloat)red green:(CGFloat)green blue:(CGFloat)blue;
- (void)playback:(NSArray*)recordedPaths;
-(void)drawData : (NSArray*) data;
-(void)sendBluetoothForSavedNote;
@end
