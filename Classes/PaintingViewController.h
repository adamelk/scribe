#import <UIKit/UIKit.h>
#import "RBLViewController.h"

@interface PaintingViewController : UIViewController
@property (nonatomic, strong) RBLViewController *vc;
@property (nonatomic, strong) NSMutableArray *pointsQueue;
- (IBAction)clickSave:(id)sender;
-(void) setPaintViewPaths : (NSArray*) paths;
@end
