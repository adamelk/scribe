
#import "RBLLightViewController.h"
#import "PaintingViewController.h"
#import "SavedTableController.h"

@interface RBLLightViewController ()

@end

@implementation RBLLightViewController
@synthesize x_coor;
@synthesize y_coor;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
   }

- (IBAction)goToSavedNotes:(id)sender
{
    [self performSegueWithIdentifier:@"gotoTable" sender:self];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

//- (void)colorPickerDidChangeSelection:(RSColorPickerView *)cp {
//	_colorPatch.backgroundColor = [cp selectionColor];
//    //    _brightnessSlider.value = [cp brightness];
//    //    _opacitySlider.value = [cp opacity];
//    
//    NSLog(@"*my %d, %d, %d", [cp vRed], [cp vGreen], [cp vBlue]);
//    label.text = [NSString stringWithFormat:@"R : %d\tG : %d\tB : %d", [cp vRed], [cp vGreen], [cp vBlue]];
//    
//    uint8_t command[] = {0x00,0x00,0x00};
//    
//    command[0] = [cp vRed];
//    command[1] = [cp vGreen];
//    command[2] = [cp vBlue];
//    
//    NSData *nsData = [[NSData alloc] initWithBytes:command length:3];
//    [self.vc.peripheral writeValue:nsData forCharacteristic:self.vc.txCharacteristic type:CBCharacteristicWriteWithoutResponse];
//    
//}

#pragma mark - User action
//
//- (void)selectRed:(id)sender {
//    [_colorPicker setSelectionColor:[UIColor redColor]];
//}
//- (void)selectGreen:(id)sender {
//    [_colorPicker setSelectionColor:[UIColor greenColor]];
//}
//- (void)selectBlue:(id)sender {
//    [_colorPicker setSelectionColor:[UIColor blueColor]];
//}
//- (void)selectBlack:(id)sender {
//    [_colorPicker setSelectionColor:[UIColor blackColor]];
//}
//- (void)selectWhite:(id)sender {
//    [_colorPicker setSelectionColor:[UIColor whiteColor]];
//}
//- (void)selectPurple:(id)sender {
//    [_colorPicker setSelectionColor:[UIColor purpleColor]];
//}
//- (void)selectCyan:(id)sender {
//    [_colorPicker setSelectionColor:[UIColor cyanColor]];
//}

- (void)selectOff:(id)sender {
    uint8_t command[] = {0x00,0x00,0x00};
    
    NSData *nsData = [[NSData alloc] initWithBytes:command length:3];
    [self.vc.peripheral writeValue:nsData forCharacteristic:self.vc.txCharacteristic type:CBCharacteristicWriteWithoutResponse];
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([segue.identifier isEqualToString:@"gotoDraw"]) {
        NSLog(@"gotoDraw");
        
        PaintingViewController *paintView = [segue destinationViewController];
        paintView.vc = self.vc;
    }
    else if([segue.identifier isEqualToString:@"gotoTable"]) {
        NSLog(@"gotoTable");
        
        SavedTableController *savedView = [segue destinationViewController];
        savedView.vc = self.vc;
        
    }
}

- (IBAction)gotoDraw:(id)sender {
    uint8_t command[] = {0x04};
    NSLog(@"Sent: 0x04");
    
    NSData *nsData = [[NSData alloc] initWithBytes:&command length:1];
    
    [self.vc.peripheral writeValue:nsData forCharacteristic:self.vc.txCharacteristic type:CBCharacteristicWriteWithoutResponse];
    
    [self performSegueWithIdentifier:@"gotoDraw" sender:self];
}

- (IBAction)resetScribe:(id)sender {
    uint8_t command[] = {0x03};
    
    NSData *nsData = [[NSData alloc] initWithBytes:command length:1];
    [self.vc.peripheral writeValue:nsData forCharacteristic:self.vc.txCharacteristic type:CBCharacteristicWriteWithoutResponse];

}

- (IBAction)drawSquare:(id)sender {
    uint8_t command[] = {0x00};
    
    NSData *nsData = [[NSData alloc] initWithBytes:command length:1];
    [self.vc.peripheral writeValue:nsData forCharacteristic:self.vc.txCharacteristic type:CBCharacteristicWriteWithoutResponse];
    
}

- (IBAction)drawCircle:(id)sender {
    uint8_t command[] = {0x01};
    
    NSData *nsData = [[NSData alloc] initWithBytes:command length:1];
    [self.vc.peripheral writeValue:nsData forCharacteristic:self.vc.txCharacteristic type:CBCharacteristicWriteWithoutResponse];
    
}

- (IBAction)drawTriangle:(id)sender {
    uint8_t command[] = {0x02};
    
    NSData *nsData = [[NSData alloc] initWithBytes:command length:1];
    [self.vc.peripheral writeValue:nsData forCharacteristic:self.vc.txCharacteristic type:CBCharacteristicWriteWithoutResponse];
    
}


- (IBAction)gotoCoordinates:(id)sender {
    
    // [x_cc.text isEqualToString:@""];
    int x_coordinate = self.x_coor.text.intValue;
    int y_coordinate = self.y_coor.text.intValue;
    
    uint8_t x_negative = 0x00;
    uint8_t y_negative = 0x00;
    int x_converted = abs(x_coordinate);
    int y_converted = abs(y_coordinate);
    
    int x_bit_1 = x_converted / 256;
    int x_bit_0 = (x_converted % 256);
    uint8_t x_bit_1_converted = (uint8_t) x_bit_1;
    uint8_t x_bit_0_converted = (uint8_t) x_bit_0;
    
    int y_bit_1 = y_converted / 256;
    int y_bit_0 = (y_converted % 256);
    uint8_t y_bit_1_converted = (uint8_t) y_bit_1;
    uint8_t y_bit_0_converted = (uint8_t) y_bit_0;
    uint8_t command[] = {x_bit_1_converted,x_bit_0_converted,y_bit_1_converted,y_bit_0_converted, 0x00};
    NSData *nsData = [[NSData alloc] initWithBytes:&command length:5];
    
    [self.vc.peripheral writeValue:nsData forCharacteristic:self.vc.txCharacteristic type:CBCharacteristicWriteWithoutResponse];
    [x_coor resignFirstResponder];
    [y_coor resignFirstResponder];
}

- (IBAction)drawCoordinates:(id)sender {

   // [x_cc.text isEqualToString:@""];
    int x_coordinate = self.x_coor.text.intValue;
    int y_coordinate = self.y_coor.text.intValue;

     uint8_t x_negative = 0x00;
     uint8_t y_negative = 0x00;
     int x_converted = abs(x_coordinate);
     int y_converted = abs(y_coordinate);
     int x_bit_1 = x_converted / 256;
     int x_bit_0 = (x_converted % 256);
     uint8_t x_bit_1_converted = (uint8_t) x_bit_1;
     uint8_t x_bit_0_converted = (uint8_t) x_bit_0;
    
     int y_bit_1 = y_converted / 256;
     int y_bit_0 = (y_converted % 256);
     uint8_t y_bit_1_converted = (uint8_t) y_bit_1;
     uint8_t y_bit_0_converted = (uint8_t) y_bit_0;
     uint8_t command[] = {x_bit_1_converted,x_bit_0_converted,y_bit_1_converted,y_bit_0_converted, 0x01};
    
     NSData *nsData = [[NSData alloc] initWithBytes:&command length:5];
    
     [self.vc.peripheral writeValue:nsData forCharacteristic:self.vc.txCharacteristic type:CBCharacteristicWriteWithoutResponse];
    [x_coor resignFirstResponder];
    [y_coor resignFirstResponder];
}
- (IBAction)clickUp:(id)sender {
    uint8_t command[] = {0x00,0x04,0x00,0x00};
    
    NSData *nsData = [[NSData alloc] initWithBytes:command length:4];
    [self.vc.peripheral writeValue:nsData forCharacteristic:self.vc.txCharacteristic type:CBCharacteristicWriteWithoutResponse];
}
- (IBAction)clickDown:(id)sender {
    uint8_t command[] = {0x00,0x04,0x00,0x01};
    
    NSData *nsData = [[NSData alloc] initWithBytes:&command length:4];
    [self.vc.peripheral writeValue:nsData forCharacteristic:self.vc.txCharacteristic type:CBCharacteristicWriteWithoutResponse];
}
- (IBAction)clickRight:(id)sender {
    uint8_t command[] = {0x04,0x00,0x00,0x00};
    NSData *nsData = [[NSData alloc] initWithBytes:&command length:4];
    [self.vc.peripheral writeValue:nsData forCharacteristic:self.vc.txCharacteristic type:CBCharacteristicWriteWithoutResponse];
}
- (IBAction)clickLeft:(id)sender {
    uint8_t command[] = {0x04,0x00,0x00,0x01};
    NSData *nsData = [[NSData alloc] initWithBytes:&command length:4];
    [self.vc.peripheral writeValue:nsData forCharacteristic:self.vc.txCharacteristic type:CBCharacteristicWriteWithoutResponse];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    NSLog(@"viewWillDisappear");
}

@end
