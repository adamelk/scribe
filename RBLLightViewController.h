
#import <UIKit/UIKit.h>
#import "RBLViewController.h"
//#import "ColorPickerClasses/RSColorPickerView.h"

@interface RBLLightViewController : UIViewController
{
    UILabel *label;
    
}

@property (nonatomic, strong) RBLViewController *vc;
//@property (nonatomic) RSColorPickerView *colorPicker;
@property (nonatomic) UIView *colorPatch;
@property (nonatomic, retain) IBOutlet UITextField *x_coor;
@property (nonatomic, retain) IBOutlet UITextField *y_coor;

-(IBAction)drawCoordinates:(id)sender;
-(IBAction)gotoCoordinates:(id)sender;
-(IBAction)gotoDraw:(id)sender;
-(IBAction)resetScribe:(id)sender;
-(IBAction)drawSquare:(id)sender;
-(IBAction)drawCircle:(id)sender;
-(IBAction)drawTriangle:(id)sender;
-(IBAction)goToSavedNotes:(id)sender;

@end
