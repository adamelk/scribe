#include "mbed.h"

PwmOut pin25(p25);
PwmOut pin26(p26);

void servo_reset(){
    pin25.period_ms(20); //Set the period: 20ms
    pin26.period_ms(20); //Set the period: 20ms
}

void servo_f(){
    //pin25.pulsewidth_us(1300);
    //pin26.pulsewidth_us(1700);
    pin25.pulsewidth_us(1350);
    pin26.pulsewidth_us(1650);
}

void servo_b(){
    pin25.pulsewidth_us(1700);
    pin26.pulsewidth_us(1300);
}

void servo_right(){
    pin25.pulsewidth_us(1700);
    pin26.pulsewidth_us(1700);
}

void servo_left(){
    pin25.pulsewidth_us(1300);
    pin26.pulsewidth_us(1300);
}

void servo_stop(){
    pin25.pulsewidth_us(1500);
    pin26.pulsewidth_us(1515);                                      
}

void servo_slowright(){
    pin25.pulsewidth_us(1550);
    pin26.pulsewidth_us(1550);
}

void servo_slowleft(){
    pin25.pulsewidth_us(1450);
    pin26.pulsewidth_us(1450);
}