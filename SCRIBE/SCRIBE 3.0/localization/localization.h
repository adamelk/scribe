#include "mbed.h"
#include "ultrasonic.h"
#include "BNO055.h"
#include "stepper.h"
#define MMPERSTEP 17

//#define VERTICAL // define it on the vertical version to compile the corresponding code

class localization
{
    public:
        /**iniates the class with the specified trigger pin, echo pin, update speed and timeout**/
        localization();
        void reset(void); // reset, used at startup
        /**starts mesuring the distance**/
        void measure(void); // measure the current position and update X and Y
        float getAngle(void); //return the angle
        void servo(int degree);
        int getX(void); // return X
        int getY(void); // return Y
    private:
        int X;
        int Y;
};