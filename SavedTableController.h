
#import <UIKit/UIKit.h>
#import "RBLViewController.h"
//#import "ColorPickerClasses/RSColorPickerView.h"

@interface SavedTableController : UIViewController <UITableViewDelegate, UITableViewDataSource>

@property (nonatomic, strong) RBLViewController *vc;

@end

